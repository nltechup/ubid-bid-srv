# BUILD
FROM maven:3.6.3-jdk-11-slim as build-step
WORKDIR /app/
ADD src/ /app/src/
ADD pom.xml /app/
RUN mvn --batch-mode install

# RUNTIME
FROM openjdk:11.0.4-jre-slim
WORKDIR /app
COPY --from=build-step /app/target/bid-srv.jar /app/bid-srv.jar
EXPOSE 9999

CMD ["java","-Dspring.profiles.active=default", "-jar", "/app/bid-srv.jar"]
