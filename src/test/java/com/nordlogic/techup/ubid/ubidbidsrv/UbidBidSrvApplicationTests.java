package com.nordlogic.techup.ubid.ubidbidsrv;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class UbidBidSrvApplicationTests {

    @Test
    void contextLoads() {
    }

}
