package com.nordlogic.techup.ubid.ubidbidsrv.events.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;

import static com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferStatus.CREATED;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;

class OfferEventSerializationTest {
    @Test
    void stringToModel() throws JsonProcessingException {
        String offerEventJson = "{\"cat_uuid\":\"595cc299bfb8a76b1387f828\"," +
                "\"product\":\"stefan-3\",\"description\":\"very good\",\"quantity\":20," +
                "\"photo_url\":\"https://s12emagst.akamaized.net/products/25730/25729239/images/res_7f421ab25b8fc32c9f556b6f0bad555a_450x450_jgo2.jpg\"," +
                "\"price\":200,\"end\":\"2020-12-20T10:01:30.010Z\",\"user_uuid\":\"72e43e4f-df19-4efb-bc56-36fe31ed1a94\",\"status\":0,\"createdAt\":\"2020-02-20T19:38:59.225Z\",\"updatedAt\":\"2020-02-20T19:38:59.225Z\",\"_id\":\"5e4ee0532fc620001022a22d\"}";
        OfferEvent offerEvent = new ObjectMapper().readValue(offerEventJson, OfferEvent.class);

        assertThat(offerEvent)
                .extracting("id", "product", "description", "status", "photo_url", "end")
                .containsExactly("5e4ee0532fc620001022a22d", "stefan-3", "very good", CREATED,
                        "https://s12emagst.akamaized.net/products/25730/25729239/images/res_7f421ab25b8fc32c9f556b6f0bad555a_450x450_jgo2.jpg",
                        Date.from(LocalDateTime.of(2020, 12, 20, 10, 1, 30, 10000000).toInstant(UTC)));
    }

}
