package com.nordlogic.techup.ubid.ubidbidsrv.bids.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.TimeZone;

import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;

class BidSerializationTest {
    @Test
    void test() throws JsonProcessingException {
        ObjectMapper jsonMapper = prepareJsonMapper();

        String json = jsonMapper.writeValueAsString(Bid.builder()
                .createdAt(Date.from(LocalDateTime.of(2020, 3, 1, 10, 10, 10, 10)
                        .toInstant(UTC)))
                .build());

        assertThat(json).isEqualTo("{\"id\":null,\"offer\":null,\"price\":0.0,\"qty\":0,\"createdAt\":\"2020-03-01T10:10Z\",\"bidderId\":null}");
    }

    private ObjectMapper prepareJsonMapper() {
        ObjectMapper jsonMapper = new ObjectMapper();
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        jsonMapper.setDateFormat(df);
        return jsonMapper;
    }

}
