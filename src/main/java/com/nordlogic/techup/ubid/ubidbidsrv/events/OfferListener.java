package com.nordlogic.techup.ubid.ubidbidsrv.events;

import com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferEvent;
import com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferEventMapper;
import com.nordlogic.techup.ubid.ubidbidsrv.offers.OfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OfferListener {
    private final OfferService offerService;
    private final OfferEventMapper mapper;

    @RabbitListener(queues = "#{offerEventsQueue.name}")
    public void receive(OfferEvent offerEvent) {
        offerService.createOffer(mapper.toEntity(offerEvent));
    }
}
