package com.nordlogic.techup.ubid.ubidbidsrv.validators;

import com.nordlogic.techup.ubid.ubidbidsrv.exceptions.ValidationException;
import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferStatus.ACTIVE;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
public class OrderValidator {
    public Optional<ValidationException> validateOfferStatus(OfferEntity offer) {
        if (offer.getStatus() != ACTIVE) {
            return of(new ValidationException("The order is not active"));
        }
        return empty();
    }

}
