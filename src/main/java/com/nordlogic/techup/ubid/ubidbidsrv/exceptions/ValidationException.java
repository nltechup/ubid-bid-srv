package com.nordlogic.techup.ubid.ubidbidsrv.exceptions;

public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}
