package com.nordlogic.techup.ubid.ubidbidsrv.rest;

import com.nordlogic.techup.ubid.ubidbidsrv.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

@Component
@Scope(value = SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class HeaderContext {
    private final String userId;

    @Autowired
    public HeaderContext(HttpServletRequest request) {
        userId = Optional.ofNullable(request.getHeader("x-user-uuid"))
                .orElseThrow(() -> new ValidationException("You need to provide x-user-uuid header"));
    }

    public String getUserId() {
        return userId;
    }
}
