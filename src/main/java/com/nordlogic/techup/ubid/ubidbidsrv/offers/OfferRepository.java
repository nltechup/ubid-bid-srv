package com.nordlogic.techup.ubid.ubidbidsrv.offers;

import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import org.springframework.data.repository.CrudRepository;

public interface OfferRepository extends CrudRepository<OfferEntity, String> {
}
