package com.nordlogic.techup.ubid.ubidbidsrv.bids.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateBidRequest {
    private String offerId;
    private double price;
    private int qty;
}
