package com.nordlogic.techup.ubid.ubidbidsrv.events.model;

import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import org.springframework.stereotype.Component;

@Component
public class OfferEventMapper {
    public OfferEntity toEntity(OfferEvent event) {
        return OfferEntity.builder()
                .id(event.getId())
                .description(event.getDescription())
                .product(event.getProduct())
                .photoUrl(event.getPhoto_url())
                .status(event.getStatus())
                .end(event.getEnd())
                .build();
    }
}
