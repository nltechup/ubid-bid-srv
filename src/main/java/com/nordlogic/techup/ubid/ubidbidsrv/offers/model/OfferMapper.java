package com.nordlogic.techup.ubid.ubidbidsrv.offers.model;

import org.springframework.stereotype.Component;

@Component
public class OfferMapper {
    public Offer toApi(OfferEntity entity) {
        return Offer.builder()
                .id(entity.getId())
                .description(entity.getDescription())
                .product(entity.getProduct())
                .photoUrl(entity.getPhotoUrl())
                .build();
    }

    public OfferEntity toEntity(Offer api) {
        return OfferEntity.builder()
                .id(api.getId())
                .description(api.getDescription())
                .product(api.getProduct())
                .photoUrl(api.getPhotoUrl())
                .build();
    }
}
