package com.nordlogic.techup.ubid.ubidbidsrv.bids.model;

import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

import static java.time.ZoneOffset.UTC;

@Component
@RequiredArgsConstructor
public class BidMapper {
    private final OfferMapper offerMapper;

    public Bid toApi(BidEntity entity) {
        return Bid.builder()
                .id(entity.getId())
                .offer(offerMapper.toApi(entity.getOffer()))
                .price(entity.getPrice())
                .qty(entity.getQty())
                .createdAt(Date.from(entity.getCreatedAt().toInstant(UTC)))
                .bidderId(entity.getBidderId())
                .build();
    }

    public BidEntity toEntity(Bid bid) {
        return BidEntity.builder()
                .id(bid.getId())
                .offer(offerMapper.toEntity(bid.getOffer()))
                .price(bid.getPrice())
                .qty(bid.getQty())
                .bidderId(bid.getBidderId())
                .build();
    }
}
