package com.nordlogic.techup.ubid.ubidbidsrv.exceptions;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String resource, String id) {
        super(String.format("Could not find %s with id %s", resource, id));
    }
}
