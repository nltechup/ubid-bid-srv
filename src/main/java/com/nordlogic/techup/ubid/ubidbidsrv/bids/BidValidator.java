package com.nordlogic.techup.ubid.ubidbidsrv.bids;

import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.BidEntity;
import com.nordlogic.techup.ubid.ubidbidsrv.exceptions.ValidationException;
import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import com.nordlogic.techup.ubid.ubidbidsrv.validators.OrderValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
@RequiredArgsConstructor
public class BidValidator {
    private final OrderValidator orderValidator;

    public boolean validateNewBid(BidEntity bid, OfferEntity offer) {
        orderValidator.validateOfferStatus(offer);
        validateOfferNotExpired(bid.getCreatedAt(), offer.getEnd());
        return true;
    }

    private Optional<ValidationException> validateOfferNotExpired(LocalDateTime createdAt, Date end) {
        if (createdAt.isAfter(LocalDateTime.ofInstant(end.toInstant(), ZoneId.of("UTC")))) {
            return of(new ValidationException("The expiration time passed"));
        } else {
            return empty();
        }
    }

}
