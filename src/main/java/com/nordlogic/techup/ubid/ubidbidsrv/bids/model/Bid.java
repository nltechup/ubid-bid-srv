package com.nordlogic.techup.ubid.ubidbidsrv.bids.model;

import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.Offer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bid {
    private String id;
    private Offer offer;
    private double price;
    private int qty;
    private Date createdAt;
    private String bidderId;
}
