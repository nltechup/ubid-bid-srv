package com.nordlogic.techup.ubid.ubidbidsrv.offers.model;

import com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferStatus;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Builder
public class OfferEntity {
    @Id
    private String id;
    private String product;
    private String description;
    private String photoUrl;
    private OfferStatus status;
    private Date end;
}
