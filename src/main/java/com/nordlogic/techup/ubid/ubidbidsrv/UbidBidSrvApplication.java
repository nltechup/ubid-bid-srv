package com.nordlogic.techup.ubid.ubidbidsrv;

import com.nordlogic.techup.ubid.ubidbidsrv.events.sync.OffersSync;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class UbidBidSrvApplication {

    public static void main(String[] args) {
        SpringApplication.run(UbidBidSrvApplication.class, args);
    }

    @Bean
    CommandLineRunner atStartup(
            OffersSync sync) {
        return args -> sync.requestSync();
    }
}
