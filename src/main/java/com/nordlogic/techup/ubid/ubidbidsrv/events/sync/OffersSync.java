package com.nordlogic.techup.ubid.ubidbidsrv.events.sync;

import com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferEvent;
import com.nordlogic.techup.ubid.ubidbidsrv.events.model.OfferEventMapper;
import com.nordlogic.techup.ubid.ubidbidsrv.offers.OfferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class OffersSync {
    private final RabbitTemplate template;
    private final TopicExchange topic;
    private final OfferService offerService;
    private final OfferEventMapper mapper;

    public void requestSync() {
        log.info("Starting synchronization");
        var properties = new MessageProperties();
        properties.setHeader("since", LocalDateTime.now().minusYears(2));
        properties.setReplyTo("offer.sync.response");
        template.convertAndSend(topic.getName(), "offer.sync.request",
                new Message("{}".getBytes(), properties));
    }

    @RabbitListener(queues = "#{syncQueue.name}")
    public void receive(List<OfferEvent> offerEvent) {
        log.info("Received sync " + offerEvent);
        offerEvent.stream()
                .map(mapper::toEntity)
                .forEach(offerService::createOffer);
    }
}
