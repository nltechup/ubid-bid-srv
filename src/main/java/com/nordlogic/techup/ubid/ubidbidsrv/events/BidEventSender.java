package com.nordlogic.techup.ubid.ubidbidsrv.events;

import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.Bid;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BidEventSender {
    private final RabbitTemplate template;
    private final TopicExchange topic;

    public void newBidCreated(Bid bid) {
        template.convertAndSend(topic.getName(), "bid.create", bid);
    }
}
