package com.nordlogic.techup.ubid.ubidbidsrv.events.model;

public enum OfferStatus {
    CREATED,
    ACTIVE,
    PROCESSING,
    SETTLED,
    EXPIRED
}
