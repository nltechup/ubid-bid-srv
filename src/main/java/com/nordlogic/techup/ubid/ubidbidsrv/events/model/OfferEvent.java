package com.nordlogic.techup.ubid.ubidbidsrv.events.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OfferEvent {
    @JsonAlias("_id")
    private String id;
    private String product;
    private String description;
    private String photo_url;
    private OfferStatus status;
    private Date end;
}
