package com.nordlogic.techup.ubid.ubidbidsrv.bids.model;

public class ResourceCount {
    private final long count;

    public ResourceCount(long count) {
        this.count = count;
    }

    public long getCount() {
        return count;
    }
}
