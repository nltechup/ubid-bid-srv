package com.nordlogic.techup.ubid.ubidbidsrv.bids;

import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.BidEntity;
import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.BidMapper;
import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.CreateBidRequest;
import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.ResourceCount;
import com.nordlogic.techup.ubid.ubidbidsrv.events.BidEventSender;
import com.nordlogic.techup.ubid.ubidbidsrv.exceptions.ResourceNotFoundException;
import com.nordlogic.techup.ubid.ubidbidsrv.offers.OfferService;
import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import com.nordlogic.techup.ubid.ubidbidsrv.rest.HeaderContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
@RequiredArgsConstructor
public class BidService {
    private final BidRepository repo;
    private final OfferService offerService;
    private final HeaderContext context;
    private final BidEventSender events;
    private final BidMapper bidMapper;
    private final BidValidator bidValidator;

    public List<BidEntity> getMine(String offerId) {
        return ofNullable(offerId)
                .map(oid -> repo.findAllByBidderIdAndOfferId(context.getUserId(), offerId))
                .orElseGet(() -> repo.findAllByBidderId(context.getUserId()));
    }

    public List<BidEntity> getAll(String offerId) {
        return ofNullable(offerId)
                .map(oid -> repo.findAllByOfferId(offerId))
                .orElseGet(repo::findAll);
    }

    public Optional<BidEntity> get(String id) {
        return repo.findById(id);
    }

    public BidEntity create(CreateBidRequest request) {
        final String offerId = request.getOfferId();
        final OfferEntity offer = offerService.get(offerId)
                .orElseThrow(() -> new ResourceNotFoundException("offer", offerId));
        BidEntity newBid = BidEntity.builder()
                .offer(offer)
                .id(null)
                .bidderId(context.getUserId())
                .createdAt(LocalDateTime.now())
                .price(request.getPrice())
                .qty(request.getQty())
                .build();
        bidValidator.validateNewBid(newBid, offer);
        var createdBid = repo.save(newBid);
        events.newBidCreated(bidMapper.toApi(createdBid));
        return createdBid;
    }

    public ResourceCount countAll(String offerId) {
        return new ResourceCount(ofNullable(offerId)
                .map(repo::countByOfferId)
                .orElseGet(repo::count));
    }
}
