package com.nordlogic.techup.ubid.ubidbidsrv.bids;

import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.Bid;
import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.BidMapper;
import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.CreateBidRequest;
import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.ResourceCount;
import com.nordlogic.techup.ubid.ubidbidsrv.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/bids")
@RequiredArgsConstructor
public class BidController {
    private final BidService bidService;
    private final BidMapper mapper;

    @GetMapping
    List<Bid> getAll(@RequestParam(required = false) String offerId) {
        return bidService.getAll(offerId).stream()
                .map(mapper::toApi)
                .collect(toList());
    }

    @GetMapping("count")
    ResourceCount countAll(@RequestParam(required = false) String offerId) {
        return bidService.countAll(offerId);
    }

    @GetMapping("/mine")
    List<Bid> getAllMine(@RequestParam(required = false) String offerId) {
        return bidService.getMine(offerId).stream()
                .map(mapper::toApi)
                .collect(toList());
    }

    @GetMapping("{id}")
    Bid get(@PathVariable String id) {
        return bidService.get(id)
                .map(mapper::toApi)
                .orElseThrow(() -> new ResourceNotFoundException("bid", id));
    }

    @PostMapping
    Bid create(@RequestBody CreateBidRequest newBid) {
        return mapper.toApi(
                bidService.create(newBid));
    }
}
