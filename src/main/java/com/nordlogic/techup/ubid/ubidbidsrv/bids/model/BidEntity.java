package com.nordlogic.techup.ubid.ubidbidsrv.bids.model;

import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder(toBuilder = true)
public class BidEntity {
    @Id
    private String id;
    private String bidderId;
    private OfferEntity offer;
    private double price;
    private int qty;
    private LocalDateTime createdAt;
}
