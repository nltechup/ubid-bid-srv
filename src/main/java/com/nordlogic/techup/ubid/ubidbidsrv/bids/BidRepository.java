package com.nordlogic.techup.ubid.ubidbidsrv.bids;

import com.nordlogic.techup.ubid.ubidbidsrv.bids.model.BidEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BidRepository extends MongoRepository<BidEntity, String> {
    List<BidEntity> findAllByBidderId(String userId);

    List<BidEntity> findAllByBidderIdAndOfferId(String userId, String offerId);

    List<BidEntity> findAllByOfferId(String offerId);

    long countByOfferId(String offerId);
}
