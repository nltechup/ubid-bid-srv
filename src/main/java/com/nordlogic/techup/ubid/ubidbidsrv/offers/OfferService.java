package com.nordlogic.techup.ubid.ubidbidsrv.offers;

import com.nordlogic.techup.ubid.ubidbidsrv.offers.model.OfferEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Optional.empty;

@Service
@RequiredArgsConstructor
@Slf4j
public class OfferService {
    private final OfferRepository repo;

    public Optional<OfferEntity> createOffer(OfferEntity offer) {
        if (!offerExists(offer.getId())) {
            log.info("Creating offer " + offer.getId());
            return Optional.of(repo.save(offer));
        }
        return empty();
    }

    public boolean offerExists(String offerId) {
        return offerId != null && repo.existsById(offerId);
    }

    public Optional<OfferEntity> get(String offerId) {
        return repo.findById(offerId);
    }
}
