package com.nordlogic.techup.ubid.ubidbidsrv.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Configuration
public class ExchangeConfig {
    private final String exchangeName;
    private final ObjectMapper jsonMapper;

    public ExchangeConfig(
            @Value("${exchange.name:uBidExchange}") String exchangeName,
            ObjectMapper jsonMapper) {
        this.exchangeName = exchangeName;
        this.jsonMapper = jsonMapper;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);
        jsonMapper.setDateFormat(df);
        return new Jackson2JsonMessageConverter(jsonMapper);
    }

    @Bean
    TopicExchange topic() {
        return new TopicExchange(exchangeName);
    }

    @Bean
    Queue offerEventsQueue() {
        return new Queue("BidService-OfferEvents");
    }

    @Bean
    Binding offerQueueBinding(TopicExchange topic, Queue offerEventsQueue) {
        return BindingBuilder.bind(offerEventsQueue)
                .to(topic)
                .with("offer.create");
    }

    @Bean
    Queue syncQueue() {
        return new Queue("BidService-OfferSync");
    }

    @Bean
    Binding syncQueueBinding(TopicExchange topic, Queue syncQueue) {
        return BindingBuilder.bind(syncQueue)
                .to(topic)
                .with("offer.sync.response");
    }
}
